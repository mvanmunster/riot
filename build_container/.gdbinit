#set auto-load safe-path /

layout src
#source ~/.gdbdashboard

set output-radix 16
set print pretty on
set print array on

set logging file ~/gdb.log
set logging on

set history size 10000
set history save on
set history filename ~/gdb_history.log

define hookpost-up
dashboard
end

define hookpost-down
dashboard
end



target remote localhost:9999
monitor reset init

handle SIGTRAP ignore
handle SIGTRAP noprint

load

c


